<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	// model
	public function __construct()
	{
		parent::__construct();
		$this->load->model('users_model');
	}

	// Halaman login
	public function index()	{
		// Validasi
		$username	= $this->input->post('username');
		$password 	= $this->input->post('password');
		$pengalihan = $this->input->post('pengalihan');
		$valid 		= $this->form_validation;
		$valid->set_rules('username','Username','required',
			array('required'	=> 'Username harus diisi'));
		$valid->set_rules('password','Password','required',
			array('required'	=> 'Password harus diisi'));
		if ($valid->run()) {
			$this->simple_login->login($username, $password, $pengalihan);
		}
		// End validasi
		$data = array(	'title'		=> 'Login',
						);
		$this->load->view('login', $data, FALSE);
	}

	// Logout
	public function logout() {
		// Fungsi logout jalan di sini
		$this->simple_login->logout();
	}

}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */