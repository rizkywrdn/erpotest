<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');

if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
    header('Access-Control-Allow-Headers: Content-Type');
    exit;
}
// Load the Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Auth extends REST_Controller {

    public function __construct() { 
        parent::__construct();
        
        // Load the user model
        $this->load->model('users_model');
    }

    private function updateToken($users,$token){ 

        $isHasToken = $this->users_model->checkToken($users->id);

        if (!$isHasToken) {

            $data = array(
                "key" => $token,
                "user_id" => $users->id,
                "created" => time(),
            );

            $insert = $this->users_model->addToken($data);

        } else {

            $data = array(
                "key" => $token
            );

            $insert = $this->users_model->updateToken($data, array("user_id" => $users->id));

        }
        
       return  $token;
    }
    
    public function index_post() {

        $username = $this->post('username');
        $password = $this->post('password');
        
        $data = $this->users_model->checkLogin(array("username" => $username, "password" => sha1($password)));

        $token = substr(md5($username . time()), 0, config_item('rest_key_length'));

        if(!empty($data)){
            $data->token = $this->updateToken($data,$token); 

            $this->_response = [
                'status' => TRUE,
                'data' => $data,
                'message' => "Login Berhasil"
            ]; 
            
        } else {

            $this->_response = [
                'status' => FALSE,
                'data' => array(),
                'message' => "Username atau password salah!"
            ];
            
        }

        $this->set_response($this->_response, REST_Controller::HTTP_OK);
    }

}