<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');

if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
    header("Access-Control-Allow-Headers: *");
    exit;
}
// Load the Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Product extends REST_Controller {

    public function __construct() { 
        parent::__construct();
        
        $this->load->model('product_model');
    }

    
    public function index_get() {

        $headers = $this->input->request_headers();
        $token   = $headers['Authorization'];

        $datas = $this->product_model->getAllById(array('key' => $token));

        if(!empty($datas)){

            $this->_response = [
                'status'    => TRUE,
                'data'      => $datas,
                'message'   => "Berhasil"
            ];

        }else{

            $this->_response = [
                'status'    => FALSE,
                'data'      => $datas,
                'message'   => "Gagal"
            ];

        }

        $this->set_response($this->_response, REST_Controller::HTTP_OK);
        
    }

    public function index_post() {

        $user_id    = $this->post('user_id');
        $nama       = $this->post('nama_produk');
        $sku        = $this->post('sku');
        $gambar     = $this->post('gambar');
        $deskripsi  = $this->post('deskripsi');
        $harga      = $this->post('harga');

        $data = array(
            'user_id'       => $user_id,
            'nama_produk'   => $nama,
            'sku'           => $sku,
            'gambar'        => $gambar,
            'deskripsi'     => $deskripsi,
            'harga'         => $harga,
        );

        $insert = $this->product_model->insert($data);

        if(!empty($insert)){
            $data['id'] = $insert;
            $this->_response = [
                'status'    => TRUE,
                'data'      => $data,
                'message'   => "Berhasil"
            ];

        }else{

            $this->_response = [
                'status'    => FALSE,
                'data'      => $data,
                'message'   => "Gagal"
            ];

        }

        $this->set_response($this->_response, REST_Controller::HTTP_OK);
        
    }

    public function index_put() {

        $user_id    = $this->put('user_id');
        $nama       = $this->put('nama_produk');
        $sku        = $this->put('sku');
        $gambar     = $this->put('gambar');
        $deskripsi  = $this->put('deskripsi');
        $harga      = $this->put('harga');
        $id         = $this->put('id');

        $data = array(
            'id'            => $id,
            'user_id'       => $user_id,
            'nama_produk'   => $nama,
            'sku'           => $sku,
            'deskripsi'     => $deskripsi,
            'harga'         => $harga,
        );

        $insert = $this->product_model->update($data, array('id' => $id));

        if(!empty($insert)){

            $this->_response = [
                'status'    => TRUE,
                'data'      => $data,
                'message'   => "Berhasil"
            ];

        }else{

            $this->_response = [
                'status'    => FALSE,
                'data'      => $data,
                'message'   => "Gagal"
            ];

        }

        $this->set_response($this->_response, REST_Controller::HTTP_OK);
        
    }

    public function index_delete($id) {

        $delete = $this->product_model->delete(array('id' => $id));

        if ($delete) {

            $this->_response = [
                'status'    => TRUE,
                'data'      => $delete,
                'message'   => "Berhasil"
            ];

        } else {

            $this->_response = [
                'status'    => FALSE,
                'data'      => [],
                'message'   => "Gagal"
            ];

        }

        $this->set_response($this->_response, REST_Controller::HTTP_OK);
    }

}