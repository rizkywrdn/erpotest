<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');

if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
    header('Access-Control-Allow-Headers: Content-Type');
    exit;
}
// Load the Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Register extends REST_Controller {

    public function __construct() { 
        parent::__construct();
        
        // Load the user model
        $this->load->model('users_model');
    }

    public function index_post(){
        $nama      = $this->post('nama');
        $username  = $this->post('username');
        $password  = $this->post('password');


        $data = array(
            'nama'     => $nama,
            'username' => $username,
            'password' => sha1($password),
        );

        $checkUsername = $this->users_model->getOneBy(array('username' => $username));

        if(empty($checkUsername)){

            $this->users_model->insert($data);

            $this->_response = [
                'status'    => TRUE,
                'data'      => $data,
                'message'   => "Berhasil"
            ];

        }else{

            $this->_response = [
                'status'    => FALSE,
                'data'      => [],
                'message'   => "Username sudah digunakan"
            ];

        }

        $this->set_response($this->_response, REST_Controller::HTTP_OK);   
    } 

}