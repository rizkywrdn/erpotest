<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');

if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
    header("Access-Control-Allow-Headers: *");
    exit;
}
// Load the Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Transaksi extends REST_Controller {

    public function __construct() { 
        parent::__construct();
        
        $this->load->model('transaksi_model');
        $this->load->model('product_model');
        $this->load->model('users_model');
    }

    
    public function index_get() {

        $headers = $this->input->request_headers();
        $token   = $headers['Authorization'];

        $datas = $this->transaksi_model->getAllById(array('key' => $token));

        if(!empty($datas)){
            
            $this->_response = [
                'status'    => TRUE,
                'data'      => $datas,
                'message'   => "Berhasil"
            ];

        }else{

            $this->_response = [
                'status'    => FALSE,
                'data'      => $datas,
                'message'   => "Gagal"
            ];

        }

        $this->set_response($this->_response, REST_Controller::HTTP_OK);
        
    }

    public function index_post() {

        $user_id    = $this->post('user_id');
        $total      = $this->post('total');
        $type       = $this->post('type');
        $sku        = $this->post('sku');
        $product_id = $this->post('product_id');

        $data = array(
            'user_id'       => $user_id,
            'total'         => $total,
            'type'          => $type
        );

        $insert = $this->transaksi_model->insert($data);

        if(!empty($insert)){
            if($type == "PURCHASE"){
                $stok = $this->product_model->update(array('sku' => $sku-1), array('id' => $product_id));
            }

            $this->_response = [
                'status'    => TRUE,
                'data'      => $data,
                'message'   => "Berhasil"
            ];

        }else{

            $this->_response = [
                'status'    => FALSE,
                'data'      => $data,
                'message'   => "Gagal"
            ];

        }

        $this->set_response($this->_response, REST_Controller::HTTP_OK);
        
    }

    public function wallet_get() {

        $headers = $this->input->request_headers();
        $token   = $headers['Authorization'];

        $datas['topup'] = $this->users_model->getSumWallet(array('key' => $token, 'type' => 'TOPUP'));
        $datas['purchase'] = $this->users_model->getSumTotal(array('key' => $token, 'type' => 'PURCHASE'));

        if(!empty($datas)){
            
            $this->_response = [
                'status'    => TRUE,
                'data'      => $datas,
                'message'   => "Berhasil"
            ];

        }else{

            $this->_response = [
                'status'    => FALSE,
                'data'      => $datas,
                'message'   => "Gagal"
            ];

        }

        $this->set_response($this->_response, REST_Controller::HTTP_OK);
        
    }

}