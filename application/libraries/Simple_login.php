<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Simple_login {
	
	// SET SUPER GLOBAL
	var $CI = NULL;
	public function __construct() {
		$this->CI =& get_instance();
		$this->CI->load->database();
	}
	
	// Login
	public function login($username, $password, $pengalihan) {
		// Query untuk pencocokan data
		$query = $this->CI->db->get_where('user', array(
										'username' => $username, 
										'password' => sha1($password)
										));
										
		// Jika ada hasilnya
		if($query->num_rows() == 1) {
			$row 	= $this->CI->db->query('SELECT * FROM user WHERE username = "'.$username.'" AND password = "'.sha1($password).'"');
			$admin 		= $row->row();
			$id 		= $admin->id;
			$nama		= $admin->fullname;
			$this->CI->session->set_userdata('username', $username); 
			$this->CI->session->set_userdata('fullname', $nama); 
			$this->CI->session->set_userdata('id_login', uniqid(rand()));
			$this->CI->session->set_userdata('id', $id);
			// Kalau benar di redirect
			if(!empty($pengalihan)) {
				redirect($pengalihan,'refresh');
			}else{
				redirect(base_url('admin/dasbor'));
			}
		}else{
			$this->CI->session->set_flashdata('warning','Username atau password salah');
			if(!empty($pengalihan)) {
				redirect(base_url('admin/login?pengalihan='.$pengalihan));
			}else{
				redirect(base_url('admin/login'));
			}
		}
		return false;
	}
	
	// Cek login
	public function cek_login() {
		$pengalihan = str_replace('index.php/', '', current_url());
		$this->CI->session->set_userdata('pengalihan',$pengalihan);

		if($this->CI->session->userdata('username') == '') {
			$this->CI->session->set_flashdata('warning',
			'Oops...silakan login dulu');
			redirect(base_url('admin/login?pengalihan='.$pengalihan));
		}	
	}

	// Cek login
	public function check_login() {
		$pengalihan = str_replace('index.php/', '', current_url());
		$this->CI->session->set_userdata('pengalihan',$pengalihan);

		if($this->CI->session->userdata('username') == '') {
			$this->CI->session->set_flashdata('warning',
			'Oops...silakan login dulu');
			redirect(base_url('admin/login?pengalihan='.$pengalihan));
		}	
	}

	// Check admin
	public function check_admin()
	{
		if($this->CI->session->userdata('akses_level')=='Admin') {
			
		}else{
			$this->CI->session->set_flashdata('warning',
			'Oops...Hak Akses Anda tidak diijinkan');
			redirect(base_url('admin/login'));
		}
	}
	
	// Logout
	public function logout() {
		$this->CI->session->unset_userdata('username');
		$this->CI->session->unset_userdata('fullname');
		$this->CI->session->unset_userdata('id_login');
		$this->CI->session->unset_userdata('id');
		$this->CI->session->set_flashdata('sukses',
		'Terimakasih, Anda berhasil logout');
		redirect(base_url('admin/login'));
	}
	
}