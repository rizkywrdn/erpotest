<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
class Transaksi_model extends CI_Model
{
	 

	public function __construct()
	{
		parent::__construct(); 
	}  

	public function getAllById($where = array()){
		$this->db->select("transaksi.*, api_keys.id as key_id, api_keys.key as key")->from("transaksi"); 
		$this->db->join("api_keys","transaksi.user_id = api_keys.user_id");
		$this->db->where($where); 
		$this->db->order_by('id','DESC'); 
		$query = $this->db->get();
		if ($query->num_rows() >0){  
    		return $query->result(); 
    	} 
    	return [];
	}

	public function insert($data){
		$this->db->insert('transaksi', $data);
		return $this->db->insert_id();
	}

	public function update($data,$where){
		$this->db->update('transaksi', $data, $where);
		return $this->db->affected_rows();
	}
	
	public function delete($where){
		$this->db->where($where);
		$this->db->delete('transaksi'); 
		if($this->db->affected_rows()){
			return TRUE;
		}
		return FALSE;
	}

	function getOneBy($where = array()){
		$this->db->select("transaksi.*, api_keys.id as key_id, api_keys.key as api_key")->from("transaksi"); 
		$this->db->join("api_keys","transaksi.user_id = api_keys.user_id");
		$this->db->where($where); 

		$query = $this->db->get();
		if ($query->num_rows() >0){  
    		return $query->row(); 
    	} 
    	return FALSE;
	}

	public function getAllBy($limit,$start,$search,$col,$dir,$where = array())
    {
    	$this->db->select("transaksi.*, api_keys.id as key_id, api_keys.key as api_key")->from("transaksi"); 
		$this->db->join("api_keys","transaksi.user_id = api_keys.user_id");
       	$this->db->limit($limit,$start)->order_by($col,$dir) ;
    	if(!empty($search)){
    		foreach($search as $key => $value){
				$this->db->like($key,$value);	
			} 	
		} 
		$this->db->where($where); 
       	$result = $this->db->get();
        if($result->num_rows()>0)
        {
            return $result->result();  
        }
        else
        {
            return FALSE;
        }
    }

    public function getCountAllBy($limit,$start,$search,$order,$dir)
    {
    	$this->db->select("transaksi.*, api_keys.id as key_id, api_keys.key as api_key")->from("transaksi"); 
		$this->db->join("api_keys","transaksi.user_id = api_keys.user_id");
	   	if(!empty($search)){
    		foreach($search as $key => $value){
				$this->db->like($key,$value);	
			} 	
    	} 
        $result = $this->db->get();
        return $result->num_rows();
    } 

}