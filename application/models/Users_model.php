<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
class Users_model extends CI_Model
{
	 

	public function __construct()
	{
		parent::__construct(); 
	}  

	public function getAllById($where = array()){
		$this->db->select("*")->from("users"); 
		$this->db->where($where); 
		$this->db->order_by('id','DESC'); 
		$query = $this->db->get();
		if ($query->num_rows() >0){  
    		return $query->result(); 
    	} 
    	return FALSE;
	}

	public function insert($data){
		$this->db->insert('users', $data);
		return $this->db->insert_id();
	}

	public function update($data,$where){
		$this->db->update('users', $data, $where);
		return $this->db->affected_rows();
	}
	
	public function delete($where){
		$this->db->where($where);
		$this->db->delete('users'); 
		if($this->db->affected_rows()){
			return TRUE;
		}
		return FALSE;
	}

	function getOneBy($where = array()){
		$this->db->select("*")->from("users");
		$this->db->where($where); 

		$query = $this->db->get();
		if ($query->num_rows() >0){  
    		return $query->row(); 
    	} 
    	return FALSE;
	}

	public function getAllBy($limit,$start,$search,$col,$dir,$where = array())
    {
    	$this->db->select("*")->from("users");
       	$this->db->limit($limit,$start)->order_by($col,$dir) ;
    	if(!empty($search)){
    		foreach($search as $key => $value){
				$this->db->like($key,$value);	
			} 	
		} 
		$this->db->where($where); 
       	$result = $this->db->get();
        if($result->num_rows()>0)
        {
            return $result->result();  
        }
        else
        {
            return FALSE;
        }
    }

    public function getCountAllBy($limit,$start,$search,$order,$dir)
    {
    	$this->db->select("*")->from("users"); 
	   	if(!empty($search)){
    		foreach($search as $key => $value){
				$this->db->like($key,$value);	
			} 	
    	} 
        $result = $this->db->get();
        return $result->num_rows();
    } 

    public function checkLogin($where = array()){
		$this->db->select("*")->from("users");
		$this->db->where($where);
		$query = $this->db->get();
		if ($query->num_rows() >0){
    		return $query->row();
    	}
    	return FALSE;
	}

	//api key

	public function addToken($data) {
		$result = $this->db->insert("api_keys", $data);
		if ( $this->db->affected_rows()> 0) {  
			$insert_id = $this->db->insert_id();
			return  $insert_id;
		}else{
			return false;
		}
	}

	public function checkToken($user_id) {
	 	$data = $this->db->select("*")->from("api_keys")
		                  ->where('user_id', $user_id) 
		                  ->limit(1); 
		 	
		if ( $this->db->affected_rows()> 0) {   
			return   $this->db->get()->row();
		}else{
			return false;
		}
	}

	public function updateToken($data,$where){
		$this->db->update('api_keys', $data, $where);
		return $this->db->affected_rows();
	}

	public function getTokenBy($where){ 
	 	$this->db->select("users.*")->from("users")
	 	->join('api_keys','users.id = api_keys.user_id')
	 	->where($where); 
		$query = $this->db->get();
		if ($query->num_rows() >0){  
    		return $query->row(); 
    	} else{
			return false;
		}
	}

	public function getSumTotal($where = array()){
        $this->db->select("SUM(total) AS jumlah, api_keys.id as key_id, api_keys.key as key")->from("transaksi"); 
		$this->db->join("api_keys","transaksi.user_id = api_keys.user_id");
        $this->db->where($where);
		$this->db->group_by("key_id");
        $query = $this->db->get();
        if ($query->num_rows() >0){  
            $result = $query->row(); 
            return $result->jumlah; 
        } 
        return 0;
    }

	public function getSumWallet($where = array()){
        $this->db->select("SUM(total) AS jumlah, api_keys.id as key_id, api_keys.key as key")->from("transaksi"); 
		$this->db->join("api_keys","transaksi.user_id = api_keys.user_id");
        $this->db->where($where);
        $this->db->group_by("key_id");
        $query = $this->db->get();
        if ($query->num_rows() >0){  
            $result = $query->row(); 
            return $result->jumlah; 
        } 
        return 0;
    }

}