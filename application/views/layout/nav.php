  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo base_url('admin/dasbor')?>" class="brand-link">
      <img src="<?php echo $this->website->icon() ?>"
         alt="<?php echo $this->website->namaweb(); ?>"
         class="brand-image img-circle elevation-3"
         style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column nav-flat" data-widget="treeview" role="menu" data-accordion="false">
          <!-- DASHBOARD -->
          <li class="nav-item">
            <a href="<?php echo base_url('admin/dasbor') ?>" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                DASHBOARD
                <!-- <span class="right badge badge-danger">New</span> -->
              </p>
            </a>
          </li>

          <!-- Properti  -->
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-house-damage"></i>
              <p>
                PROPERTI
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item"><a href="<?php echo base_url('admin/properti') ?>" class="nav-link"><i class="fas fa-table nav-icon"></i><p>Data Properti</p></a></li>
              <li class="nav-item"><a href="<?php echo base_url('admin/properti/tambah') ?>" class="nav-link"><i class="fas fa-plus nav-icon"></i><p>Tambah Properti</p></a></li>
              <li class="nav-item"><a href="<?php echo base_url('admin/properti/tipe') ?>" class="nav-link"><i class="fas fa-tags nav-icon"></i><p>Tipe Properti</p></a></li>            
            </ul>
          </li>

          <!-- PENGGUNA  -->
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-lock"></i>
              <p>
                PENGGUNA
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item"><a href="<?php echo base_url('admin/staff') ?>" class="nav-link"><i class="fas fa-table nav-icon"></i><p>Data Pengguna</p></a>
              </li>

               <li class="nav-item"><a href="<?php echo base_url('admin/user') ?>" class="nav-link"><i class="fas fa-plus nav-icon"></i><p>Tambah Pengguna</p></a>
              </li>
            
            </ul>
          </li>
          
          <!-- MENU -->
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                KONFIGURASI
                <i class="fas fa-angle-left right"></i>
                
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item"><a href="<?php echo base_url('admin/konfigurasi') ?>" class="nav-link"><i class="fas fa-tools nav-icon"></i><p>Konfigurasi Umum</p></a>
              </li>
              <li class="nav-item"><a href="<?php echo base_url('admin/konfigurasi/email_setting') ?>" class="nav-link"><i class="fa fa-envelope nav-icon"></i><p>Setting  Email</p></a>
              </li>
              <li class="nav-item"><a href="<?php echo base_url('admin/konfigurasi/logo') ?>" class="nav-link"><i class="fa fa-home nav-icon"></i><p>Ganti Logo</p></a>
              </li>
              <li class="nav-item"><a href="<?php echo base_url('admin/konfigurasi/icon') ?>" class="nav-link"><i class="fa fa-upload nav-icon"></i><p>Ganti Icon</p></a>
              </li>
            </ul>
          </li>

          <!-- PANDUAN -->
          <li class="nav-item text-danger">
            <a href="<?php echo base_url('admin/manual') ?>" class="nav-link  text-success">
              <i class="nav-icon fas fa-question-circle"></i>
              <p>
                PANDUAN SISTEM
              </p>
            </a>
          </li>

          <!-- LOGOUT -->
          <li class="nav-item text-danger">
            <a href="<?php echo base_url('admin/login/logout') ?>" class="nav-link  text-warning">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                LOGOUT
              </p>
            </a>
          </li>
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">
              <?php if($this->uri->segment(3)!="") { ?>
                <a href="<?php echo base_url('admin/'.$this->uri->segment(2)) ?>" class="btn btn-outline-secondary btn-flat" title="Klik untuk kembali"><i class="fa fa-arrow-left"></i></a>
              <?php } ?>
              <?php echo $title ?>
            </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dasbor') ?>">Dasbor</a></li>
              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/'.$this->uri->segment(2)) ?>"><?php echo ucfirst(str_replace('_',' ',$this->uri->segment(2))) ?></a></li>
              <li class="breadcrumb-item active"><?php echo character_limiter($title,10) ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">