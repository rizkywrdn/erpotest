<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title ?></title>
  <!-- icon -->
  <link rel="shortcut icon" href="">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/sweetalert/css/sweetalert.css">

  <script src="<?php echo base_url() ?>assets/sweetalert/js/sweetalert.min.js"></script>
</head>
<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <a href="../../index2.html"><b>Login</b> </a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
      <div class="card-body login-card-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <form action="<?php echo base_url('admin/login')?>" method="post">
          <input type="hidden" name="pengalihan" value="<?php echo (!empty($_GET['pengalihan'])?$_GET['pengalihan']:'' )?>">
          <div class="input-group mb-3">
            <input type="username" name="username" class="form-control" placeholder="Username" required="">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="password" name="password" class="form-control" placeholder="Password" required="">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-8">
              <div class="icheck-primary">
                <input type="checkbox" id="remember">
                <label for="remember">
                  Remember Me
                </label>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-4">
              <button type="submit" class="btn btn-primary btn-block">Masuk</button>
            </div>
            <!-- /.col -->
          </div>
        </form>
        <hr />
        <p class="mb-0">
          <a href="<?php echo base_url('register')?>" class="text-center">Daftar</a>
        </p>
      </div>
    </div>
  </div>
  <!-- /.login-box -->
  <script>
  <?php if($this->session->flashdata('sukses')) { ?>
  // Notifikasi
  swal ( "Berhasil" ,  "<?php echo $this->session->flashdata('sukses'); ?>" ,  "success" )
  <?php } ?>

  <?php if($this->session->flashdata('warning')) { ?>
  // Notifikasi
  swal ( "Mohon maaf" ,  "<?php echo $this->session->flashdata('warning'); ?>" ,  "warning" )
  <?php } ?>

  </script>

  <!-- jQuery -->
  <script src="<?php echo base_url()?>assets/admin/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="<?php echo base_url()?>assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?php echo base_url()?>assets/admin/dist/js/adminlte.min.js"></script>

</body>
</html>
