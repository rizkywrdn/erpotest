$(function() {
    var names = [];
    var selected_files = [];
    $('body').on('change', '.file-image', function(event) {
        var getAttr = $(this).attr('click-type');
        var fd = new FormData();
        var files = $(this)[0].files[0];
        fd.append('file',files);
        var files = event.target.files;
        var output = document.getElementById("file-media");
        var z = 0
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            names.push($(this).get(0).files[i].name);
            selected_files.push(file);
            if (file.type.match('image')) {

                var picReader = new FileReader();
                picReader.fileName = file.name
                picReader.addEventListener("load", function(event) {
                    var picFile = event.target;
                    var div = document.createElement("li");
                    div.className = "file-gambar";
                    div.innerHTML = "<div class='media-gambar'>"+
                                        "<img src='" + picFile.result + "' title='" + picFile.fileName + "'/>"+
                                        "<input type='file' name='image_upload[]' files='" + file + "'>"+
                                        "<a href='javascript:void(0);' data-id='" + picFile.fileName + "' class='remove-pic'><i class='fa fa-times' aria-hidden='true'></i></a>"+
                                    "</div>";
                    $("#file-media").prepend(div);
                });
            } 
            picReader.readAsDataURL(file);
        }
        console.log(selected_files);
    });

    $('body').on('click', '.remove-pic', function() {
        $(this).parent().parent().remove();
        var removeItem = $(this).attr('data-id');
        var yet = names.indexOf(removeItem);

        if (yet != -1) {
            names.splice(yet, 1);
        }
    });

     $(document).on('submit', '#file_form', function (e) {
        e.preventDefault();
        var fileInput = selected_files.length;
        if( fileInput > 0 ){
            var formData = new FormData();
            for (var x = 0; x < fileInput; x++) {
              formData.append("files[]", selected_files[x]);
            }
            $.ajax({
                url: BASE_URL+"admin/properti/uploadImage",
                method:"POST",  
                data:formData,  
                contentType: false,  
                cache: false,  
                processData:false,  
                dataType: "json",
                success: function(response){
                    if(response.success == true){
                      console.log(response);
                    }
                }
            });
        }else{
            console.log('No Files Selected');
        }

        console.log(fileInput);

    });
});