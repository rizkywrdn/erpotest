PGDMP                          x         
   db_testing    12.1    12.1      &           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            '           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            (           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            )           1262    16393 
   db_testing    DATABASE     �   CREATE DATABASE db_testing WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Indonesian_Indonesia.1252' LC_CTYPE = 'Indonesian_Indonesia.1252';
    DROP DATABASE db_testing;
                postgres    false            �            1259    16435    api_keys    TABLE     �   CREATE TABLE public.api_keys (
    id integer NOT NULL,
    user_id integer,
    key character varying(40),
    created integer
);
    DROP TABLE public.api_keys;
       public         heap    postgres    false            �            1259    16433    api_keys_id_seq    SEQUENCE     �   CREATE SEQUENCE public.api_keys_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.api_keys_id_seq;
       public          postgres    false    205            *           0    0    api_keys_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.api_keys_id_seq OWNED BY public.api_keys.id;
          public          postgres    false    204            �            1259    16455    product    TABLE     �   CREATE TABLE public.product (
    id smallint NOT NULL,
    user_id smallint,
    nama_produk character varying(50),
    sku integer,
    gambar text,
    deskripsi text,
    harga integer
);
    DROP TABLE public.product;
       public         heap    postgres    false            �            1259    16453    product_id_seq    SEQUENCE     �   CREATE SEQUENCE public.product_id_seq
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.product_id_seq;
       public          postgres    false    207            +           0    0    product_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.product_id_seq OWNED BY public.product.id;
          public          postgres    false    206            �            1259    16629 	   transaksi    TABLE     �   CREATE TABLE public.transaksi (
    id smallint NOT NULL,
    user_id smallint,
    total integer,
    type character varying(11)
);
    DROP TABLE public.transaksi;
       public         heap    postgres    false            �            1259    16627    transaksi_id_seq    SEQUENCE     �   CREATE SEQUENCE public.transaksi_id_seq
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.transaksi_id_seq;
       public          postgres    false    209            ,           0    0    transaksi_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.transaksi_id_seq OWNED BY public.transaksi.id;
          public          postgres    false    208            �            1259    16419    users    TABLE     �   CREATE TABLE public.users (
    id smallint NOT NULL,
    nama character varying(50),
    username character varying(50),
    password character varying(100)
);
    DROP TABLE public.users;
       public         heap    postgres    false            �            1259    16417    users_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_id_seq
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public          postgres    false    203            -           0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
          public          postgres    false    202            �
           2604    16438    api_keys id    DEFAULT     j   ALTER TABLE ONLY public.api_keys ALTER COLUMN id SET DEFAULT nextval('public.api_keys_id_seq'::regclass);
 :   ALTER TABLE public.api_keys ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    204    205    205            �
           2604    16458 
   product id    DEFAULT     h   ALTER TABLE ONLY public.product ALTER COLUMN id SET DEFAULT nextval('public.product_id_seq'::regclass);
 9   ALTER TABLE public.product ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    206    207    207            �
           2604    16632    transaksi id    DEFAULT     l   ALTER TABLE ONLY public.transaksi ALTER COLUMN id SET DEFAULT nextval('public.transaksi_id_seq'::regclass);
 ;   ALTER TABLE public.transaksi ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    209    208    209            �
           2604    16422    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    203    202    203                      0    16435    api_keys 
   TABLE DATA           =   COPY public.api_keys (id, user_id, key, created) FROM stdin;
    public          postgres    false    205   �        !          0    16455    product 
   TABLE DATA           Z   COPY public.product (id, user_id, nama_produk, sku, gambar, deskripsi, harga) FROM stdin;
    public          postgres    false    207   !       #          0    16629 	   transaksi 
   TABLE DATA           =   COPY public.transaksi (id, user_id, total, type) FROM stdin;
    public          postgres    false    209   �j                 0    16419    users 
   TABLE DATA           =   COPY public.users (id, nama, username, password) FROM stdin;
    public          postgres    false    203    k       .           0    0    api_keys_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.api_keys_id_seq', 20, true);
          public          postgres    false    204            /           0    0    product_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.product_id_seq', 92, true);
          public          postgres    false    206            0           0    0    transaksi_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.transaksi_id_seq', 39, true);
          public          postgres    false    208            1           0    0    users_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.users_id_seq', 16, true);
          public          postgres    false    202            �
           2606    16440    api_keys api_keys_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.api_keys
    ADD CONSTRAINT api_keys_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.api_keys DROP CONSTRAINT api_keys_pkey;
       public            postgres    false    205            �
           2606    16463    product product_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.product DROP CONSTRAINT product_pkey;
       public            postgres    false    207            �
           2606    16634    transaksi transaksi_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.transaksi
    ADD CONSTRAINT transaksi_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.transaksi DROP CONSTRAINT transaksi_pkey;
       public            postgres    false    209            �
           2606    16424    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            postgres    false    203               ;   x���� �7�9���K>:b�%d�!�2�g����{40�q(6ꅅg����kK"      !      x�욷���ҝ�˧�2�i�UCy�����\4hѡ�����]Y�+��.�L������FP&��'[���>�`3���&kA`��c�mc��1,c3���p �cHV�w�o3�b�ޒ���Et;�OgH�����~n�
����ȓ�a�<�$tp��~�Ǩ �2��p�O��q���_����{V�3?̩�oj�ߪq6��d�6�}.&�Y�د/ا%\�@`������w,[x�Q����4α���v������~��9��ad��u�)A�a�7^`�������e~:�ڑ�aZ���j�7�2����������+m��2���ו�ٱ��DvVg�}�b��|�Ϛ��w�N�3�l��O�"�&�꧝��7[�w���������(�&�>F�)(c��:⇹B5r�n��~7Q��H��4���J��؞.���k-�;[\��|x+#$_*l�Z�H�f���
lp��'>oLt���)�_�����{�iK������u�u�t;rm������Fe?��i�:���a���1P8�RJ��1z#��#l4fz���@��v����0�$B�.��1	�'�|$G+�U����j��T�S���븟�fR�~"�i^�g�d#k|���C���46� ��ٵ7���8���$sro�}��|�(������1�q�� ���B=��Y�1.t�x��q!?��@`E@-k)�qEv?E��76uUR=Ew�L�Z3�"�@���a�}(��)��6�asB���6|Qt�%�ބ���4����
� D[��G��H2\��Y���/�nF� ��,�����'i-}/�=��_��Țc�)�6{4����G(�\�m�`��u8�������X���@�N}k��@�-�G&|�UB��2(Ѫ��?6QM�O��2����������l��`,���OX��"*�ӄ~�"��it�(� �\�#�V�Q2"�F8t���P�-�����Esz­$C�j$l-�ŵ�r����o1�Mu����>���>��=i��4p�
�ַ�o��]\��/)��H��d�%R��\Ph|r�r�.}������|��0�ۿˠ\C��ؾ;�����ׯu/���Խ<�f�3�L:��`��̨�aKS��1�H�Bm�VT�E����N-�m�]GCR%�Q�C�]F^�?BE����S.#boa���R�hcyM0��T�Zx4
H2��Y�N7Y�/Z���!�hfi6�%+�0�9�7��A}K$T,6}���3��&=���ڬ��(�	�Mu��Pdު}��g)d�σ�|S����b�P,��3���X���Mc���C�� ��,�5b ��LH���p`�S
�)L�W��GƬr�G#Af_�(%�%R&�f��|9�E��lN�\�Z
���DG3a3 !#�@��{Z-$��'�.R�o�>��y�n<U\Pl�{d!%���B����5k��.`�aӆU�H,N6)�;>�"����e��������K��	:��[�cs��|�%N@#��%p�L_�����w�iQA���?� ��E�H沘�FޢO
(�k ���V���{��1��]x��L�8�!_����J�ٞx�8�f���'5�b���6 ���Nn��	#�a��z�糲�fG���G��%�nk�%{���F��ՙ��N� ��H/+�%�o�H{��j��^0���H��F���Rs����7 �+�}��*Q��J_���gtVM�/3�o�Nu�NA#�p׻��X~�/˩Cp:gAMNR�H��>/TC?�)�����6'S/�h�������x���#N�k<^���<��>-h��$�8N�v�DAj�]75�/Z!&���1P��?>�{�j��8���?bo/��~�ZǕ��y_����0�G������u~`8��~�
�%E' {�"�o��K�x9<K���"���GU����*��[�Y�+��;���|rW��pYX�0��Q~��mKo�y��+u\���e-�W�c{?%�6E�7_38'^dPL�Cu44��s(U�لc�����`���s[ҧ~���byal	e$1V�ݯ��;�2��h�b�1]��2<�J�"���`48�$1&���[��������ԺTِn/%E�3l��$��/����	&:����H_|����U�_��Q���E�5�4���n>�b�K ��DGb�{�A�)B��t/k��I?�g�Ҟ�q)�����	ȅ�Dh�n��P5����n��kL���TĆ��)��-כ�ܰ6�ʴ��4?�&	��|l�q�|�������Y�LL�
����L�˃�d�[�Ƿ��47R\�D��1 9���#5��X}<)ꓬ ϭ�����2�,��ti}I���-K��|�������b�(���\p5
����fdԁ=9�U�'���F���7�'Ì΢AX�T׭n�]�m��0��fc¡m�r�(Mh�M��j�60)(���	�rCnw�`a��cNk��.�
��ϞY���'_�L�=22*�Ͼf&���m$h�r�S��P�����$������\�+�'��i|D�W!]��g�"T�~v6c+&]&6�(=)���d�
�gH #v�I��;(�գ���>�F
w(&q0�b�����[���a�{�l��[�v-��۹ޱc�1��}�{��&��/�3Ug7h����E��Y�Ʒ@��~!�V���
 ��Z�0�N�R����H�h����4L��iw�]�
o�+B	���(�oJ�{���Fnpn�{������T��!�؅��q��j�k��m73|\�a�t��-}��d�j��%*�	��Tq��[6�O����i§�6�L&�{��$�P��:�� �",�[����ҷDf��rȌb% gCmҮxUE��l
����;%��������o �s�v���mJ	��n�>��:Y��`���`�&�"��ţ����-��) @�,�&�"�����{1Z~�[c�g�oQ�#lf^�XE��,�a5.�+�W{*iK�L\ƒ	V�܌|?��	
ƅTr������x�Q( t)��iu̧nG�5=�&)�Thk܄3�`隟��O�e(��r�:�F:�kLg/%���voV?c��'�L9��Sp�q��ˆ�25�(����~>>���)�G���C�̡0Be�_�ܖ�)�%q����3���t"a\�-�Ǘj��%��q�V}LL�eʍ$�y��� ӐZ�q�e�a�W3d�(�`L��U4�IL(��J�7����]V�������/���d�g䴄�e �/�\!*@4l�|CuX�ȼ-Cl�"e�d��,Á-D�M��Un?p�Qx��C��ms�lkeإ(�u�.�(c�	�����������&��_�*X��)���m�Y�ӻ�UB_�����u/؀\�E1')a*�%��-�s�����o� �]^K8t�O͞�]�.IdPEgr]�Tw�G�"���_���`�� ����rC�f����V���^#�mD���B���=^t��P��.�;V�!�-w���ͻ�����7w����4�N�+$��as����V-�Ԍ4U� �J:(�\v�4o,���bC�G�@F�BI�UE4�)V�F�-�!��oТh� �PE��ة����Xo�D�!�	Z��l�W���|�\�@����5V���7v"^���Vs�`�G�t���I��m�k�F��r�#O^g�؆0:���	́9&�@S�����.��W:z�2�l�(�H!��~)��0�����>X\�u-C�
H�B%����Ns��~;��p�'����;ŲR|������Txp�-%=lA>~�m���ͥ&W�,:O�*^u�M��Y�t�۫&]e>:]��D%���ܨ����5+ k8�>�n��Ԫ� ���6�z� -��u�WI�߾SA������j>��7��s��	���P@E9z]���0ƮK�g�=��J�ra�oĝy�~��b��G�
@���^쇴�VB,�N���=����mQr���    <��k�>.�~���� |$�D�Ģ^�6}r���#�R*(0.�pnX�@��J�����I��㤠񑄉%�k:l�r{����\jy�s˴��D�SjG^3:��=ӝ��)Z���Q��dU�5U����}ؤ���K�/�v��^w|����G,<<����wd�Q�Ws@�Х;�W�K���q�(��_���<��_�h�?��	�2j��	n��n��:�YAF~�r Pol�c�;Qͬ�X�W����/���(��l��:�
*�I����Ң;�x��s�ԲxE���� $��>����D���-�}%F4w\�� �"LWIŮ��:1�x��!�?
l�����tC��y1�ka0��-�9w�]p�O=���&�HQ�k͞�O2rF�F�vCt}M��1�͹ ��4��s�/	\22z�ާ�u6r�m�{�d��p�֝GJ���P�[S������l)lkٯ�kČf��=�����sC&��s־��s8��-�V�q�@0��(4[ �����-�����L�x>E��)�
���H��Xg���S�Z�0��/_0پ>�=w>_v�.1�D�y�˙�A V�`�7 �M�n%�]�1�{P���q�]�#ƨt6�˗F��}��vx�f�RD��#<]I������J~8�7N��.@�K.<G7 
�H��O���盐L�\�emA`�y����30F�Jf���/j�Ղ�w��}�ԸV���ֹ���/S�G��� �J'�v��P��ur���&@HmF���>��h����vS�_���(3W)��#ȀB\��rH8 ^$A�_�)/�D��
ĳK��(YԜeXnW��SP�ȧ�g^�<�F�Np�F!�)�ukI~^��T�'�?��-�Z����05O�+/}k˚���/�n�7R|}Y��Eˮ���
eX�9<I�=����;�q�\�`�a�-P:e]r-���}�{�4/�b��];x�6tb�6E�L������.��8������	w<��k�
�;-	#��<T�J~��ŲQ#���H����+�:�IT����"�Y�I�<�q}���!X�L@��R<<��.��^m�$)�����,e����D1�<�s�o/Y4lFВ0��"|٠�e�w�����
����it������藚��B�y��n2����_��\"�B�C�Ȅ��f���р�'m;0��d5�r��g`��Q?B�u>�TL�Pv�^! �k4���9���F- ��q�=$�0pyIsY�j.Ē�K�x����e9R;���h��h���,���£�C�7'�Gt�D�O�Rr������K;HQ��iL�i��^F���nN��ث-#ݜL�1>�+��G����oŦ���T�����K�Q���?��`���f(=v������S�wk�O��%3���,�=�q�X��H=@����-���Eq~ʩQ^t0χ �_c�|->���Yj4�� �!���(P�������v�%�.ݝ���y�M"�Q{؋�I^�d<�2Z�#B'l�f/?�{9�T����Y�Os�
�v�N�g�(�&L����O�5(j�<�8c�Fe/�/}���������ƴ]���'�#^ʴ������r-���!��B��R�
��D��H��#���)N�����/����>�R-�W�:�F���Dn�
۔>D5қf���p�Aq��w�4eь�p�2���B���~���#*9��(��/@�Ӱ���q�Y�&�,����L �]���7�h=8蓧ȍ7֜��?�^-f;�t�^���_�JL�:؆��	 (ڍma\1�XA�с���
kWOd��Ɛ8��\[;I��-�ɬ�?��O�6`����
7�m6��T��L��#��_}��&@��]T�����%��@QR�3Qj�,�''�gmK[����}>��$ڋ�����<K "���`�U����̧eq�G5�
��eS�2C��:�=4K6~.�I�(��$f|���ܣ^�t��hfL�"��`o$�hJ�z�E��ō�4\X�6椇�cf�������4Gu�2��4�S~������v�{�0�YDR�BF��M������u#�ںh<�l5���XPdN¯v@ŦMS�:<��^�����?�^���6��)Fp%�&F�-��@w���Q�_k�a�d��M�`_5�([��Ja�h��m�p�&�|�)!LWWgU�/��	צ��RB3+~ ��E�Zi�4�H�5@|�f]��0�u�Icq ��z�rF��I�	
R<AL�%�b!����F��ؗSY'g(��8�=������[�H!A$����ZR��p^Mr0�g���$!y$W�~vsE0�ّ�qO��b��&�c�[�����q*n��m++�
�D*�>�a���
m��^"��l�(�:����;�Uץ$�2F'�����;5�g1"b�W����( ����`���ؕy��#��-YU)�^�`�<$%K�C%�	�}�<S��Nf���m��Q�@�!��P�Z5�<# �d�{0���α�Ldݟo��C�3���g�
��U�`���8@\�MG�j��0�k@�-����j�k$��1�����cng����Aq�j��NA��	�@/i@(mB��Ƌ��
Qsz�_���6����#Fb�.g���w��}LFR"�B����M���t�'nvno~�h�� �>mP*�ec��6km�p��	��y�����uZ��\�;3r�
�'�.�-�mW�#p����V˟ֽ7�| A���<���)����7L�j�2a��������;i�ҷC��B.�Hi1ǐ�7�y��k�ן�#�_Zo'���(��V������詹-/����:D?Qba�6F[�64m�	�Q4�3,���G0U���x���u_��1�N�_/�M��wk%�S'L��a�#$JhZ�|G���?.�@m�K��G+��&ћt�yaZʹ�z�k���4�z�� k�$Ӫ�R��dL�t[�����ш`BR���`\d����n�s������a����7��T	BSV� ��k:��O��5��E��u8��m,���������\��h�o�W���^�����c��l���^��o�8.���L�����˰�i-+wĘC��mI�02*��lva�^
���8��S�7�������9��WG���e88�bT�ÖBy�9Ѽ���sk��_���Y�)Ȅ4���	�4E��􎆤�{x�W�
���W&:z�j,��/Ǭ�2I��u{2�%}��?x����{82�Q���M]�h*'�5������DȮ�k�e��R��_$�c�Rzhq@$!;o2�_�� F�&]m[�q,�p����_���o�����D3�d�RM���* ���7zD�0IR��'N��@4�7��,q�n˵(�B�,Q�֓�e�� �p���=�cVrT��x��ͣ4��P�M�y������V�t�=��u���Lѩ�x�[��b���Y'֯Z>�X��;i2��C�D���蕾UL[��c,:i+('B�9��0�m��L6e9�۲�C ��s���uۦkL���%v�M7x��9�OR��zk>���[ۯ+IL�X��������\Ǝ-S<�.F+��$|�GS�0�y|怾��ca�@W�	D0^Q�
�,r���u��ӯ�%<�h�����%�,��=�]D����F���c9�u�)Zl}%��3o�χ�F*u慿3��	��B�s�t�c�.iZ�M����+����j��y64��k�݌��Є����i����]���IT��m��\q��T�e�P�f�ۼ��z�����Z���ܖ�B����q�3#fX��}B�(�\I@�����@��k�j�,î'��K0��i���>~�з�<;
"�}���l�N�d�y��
�cr~m�׿�sՉ��a,�_��Jq8m�/X[�ish��2��ַ���w-�@wJ�{Hֲw��<���	��8����Y��C�Q��n<aV��    v��H��5�V܄X4(υ�J���= �����C���[���Ma�����  ��i�fsA%��4��^�QJ�Cg0�k����֫��g��/[ܛ��C�����$��|tS	%���#$��x�W1����G?���iu�XW)��T^������aR�d�\�>K�U_���!�����^�56����/'C<�@NS��_G����_��������%y����y�K���4}'Սq�!,���S6���~��a m ���y�/tYύ�H�D�	�]NJ��6�"���6���E~ W��K�	C��켬-�H���ۈ�C	�<!���$�v�Q�{x���U]l8DA�@����4ʇ�_/��~�趹F(�h��>�E�+�T�X*��V���f���yV]3��/�/&�1�h����k�p�f��E�{WW��o��-+�-}h�T�v�� �=넂�0x�u�P��[�L�u^W����~�� {C�2�<��X��m�FWs��%H��<�����!O����!�W/�g�ԇ�#�iP��]�o�hL��PM��5]��:ý}�[[��L#Q%H�DЖ������a��+�����K+�@�7�?�舋Z�[��R����Ȳ���x�$��E�i�P��CSD��qf�g��1�~����? ��� ��7B0���/�~1�JF18\���/T���e���`�w�,����Q,9�a���S���9��;4UT��s	�R_-�	�%T�U���C�f�w���;�O��9o$x&#����z���Ƥk�"�WU�);����I�FsgB�t����J@��֍�Z�����)�l
�r�7a�b>"ZX��v���
��K�����"y]���=* 6����/�P��y ��kL+A����3��{9��"3`z	�o�YR���+����B��>b�^���l▨b�M��$�ton�<BM
�L��EK̚�y�T�_/*Z��i��/�@Bm�Rj-g/<��s.�I�ݾy��|�>>dLr	�<��XE��>��z��\����ܻ��|�Ê4�
�y���_�3M:�	��P���C���ق��W� <�r��HM�w�G�CT��"�D�����r�1�x#�}J:O�ٶ�e�0�*J�Ę� s|�������Z�t�e��z,�S����	�ɓ��#8�1߈+Q�_Ƚ���'��i�虜I��������g��5L�������5�Ő��A��-�@R	+�Q;��M|�;)r�YW6�۴���i����v!E���635Ȃ �c��� !� �ܥi)R�|7�L�D-;�����o��D|ǹ�����<�L�+h�C����Ya��I�\>���2l�,$I������s��2`]>Ch�,�^�6Ua?q�k&��ٚ�B�-5�e�ϸ�%�W:ms%��nR�υ+��hZ5���?�:7�\��%�AI4c��ڌ���µh'�ۘ͡箺W57"W�Gg��"}� ���RY:��o����3�V��'kjc�d+#3�n�hF{�F~�� �p�4����Q��_�˒��8�!=����������;a�ڲV���g�K(`B�����& :_y.%�X�N gv;a
׆��$tI�8�)D�n#$��!ѹ����]�z���j�$�3�R��Xq�3'�ݩcr�zt�q����(��O>1c\$-�+�I@�' t���৛&�j�3��W'lT&�א݀!���cpk׷�>�O��ܙ�k��f�0��,!�gyM��R��+�Cc���������}�h%F�v���A,����ݪ�t�L��:Z�4K���ub����pd
�ŵ|0C��`t��[O�8��#t�E�
/���^��O���y�T�m��b��PG�E6�~�KU���{�8Ջ����j�f;&}�y�@�	�����fa ��*��i�i~�R�;�}@ڼ=��?�kNƤ�B*+x�q����j��׺YQ�s��.)�',��$���4���x�k�'d��^cw$���J��\>nHs>΅�-E(�Q;���}�0^�،��5rX�fܷ)B�O�<�K r:Lڪ�*����>�8�����`ς,y}58��i���I8���:Q��u��X�&�����o+��Հb��_d�ú�|�8@�l�v��9��5��9� �0���T�L��n4��E���l��N2��2
p׵�nOo����#�� FO9d��a5�m���Ma� ;���\��ۘ�)�`���$�J�8��=���n��e�J�,P��m�C��y�e+tQ#�O؆�}\<`eBO���aS&�K����[3?^C0!���L��@q�^(Vn�GmQV�A���2vZD��-׎�DҘ��{�.�ۇ#�
~�b8ڟ�i�x��^(�$h�6�'����ҀHP�C÷�L�zՙvy-L3��Q������.�e=wo߆���%.�i�pv8�t.
��D˩���'w�I2(��J ��-n~(ýi����K`�1�j���u:�;��S�7�3 ,�7�Q.�:L:[J܉� k,�C��i<�sX隷[E�~�Β�|Z~�>�l������64�"g�*��s�	�]Dh���h�b!�ξ�ZS��G!?G2�YnG��0�.yd��ѯ���(�P���X0�B~�ڴ�����+�pG�il�C._*��3�.�Mv���E�[�#��m�&H1�w�~&b,Ҭj��D�i�����i�p�H�Q�)��7 *�^w%��}�Q�|~��z��<�j6�<{a�܇�*�a���WV0ܯ^	���5�A@�ߧj'�{:ߊ�)c�di7q�Ȁrs�-�F��ekQs�7�����
l��ݷ�AF"3a��N���a��4k/��jl���|�p�r2.�X��5����e;>�f9~�o��� �~��tEH]����n�c��vP��B߬�2hI]*�:�-�`Q��WQ�=����KG���T��_�cK*�r��&ݏy�� /5)a[�u}���A�?��H��s���E������?f���-\`�b���W�畴�K������Qq�>��֏�|�<Z����q��Z~�AH�� 5�F��hl��0�M�H���G�'�����L���a�ގs�;�) � l��b/��u��&��R�A�;=		4����N?.vJ/�ZH����M��|�?�Y܁�`5HRiצ�l�7�E� ��b��@n�KQ�N0	͜�}n�0�iGG�3��턉l��g�Pl�KΪ%o��z#{7ti�>W��Џ�w- �K��10��w�����x����Y�X�]�z�JN
�)�JOr�Np�^1���+�/d������	�le_���;humx;3��R�焍�):��j�yL}�I��7�}~�nR�H 6T�������|���\$E�U���{��e��LH��ܟ�C���w�aO�����������͒��:�$��
�����[���Jo�._K nf�~|�ʧj���HP"{�!����{/�z��*����!�S�Î��#@fX��lٕd��;UCN���p��\~��(z���m�ҙ��H�I��~Ձ��zӖ?'�Rd���:n��)��.Q���j���xo ��\j��4-�|	�4f���]�4B|�O�i����n��ƈZ�DڍS�ڱ�8�ȗ��0H�l���m�����AIb���)�X�D)x0�������̳��̟�;<�
[��mDbY
E�Z�j��K��w釕���XI�gk&r.0�`
��ʟ�WIB'�����_̄x���I�Sc�g�����1k�@��X|N�F(�I_@	 � �$�٨&�}�}	�:�B�G:!P�,x���-��Om��8J�|�o�:�K�7×�.�^l
�L�7O.q��?�-7;�?2� ���������,��)E�"_sH��*h(Lp�-�y�/BiU8�%���U��$N�2��;��hBl_%S��NJf���g�Ԁ� K��<�G�*�#�jp�    �a-�`w���a/@�Zm��`�W�=�u�s��]�� ^B��t��N��
�*1��K�M N�2o~r�z/ �/e^~=�+�2�3��-��ę2�����@D��E�M�z@�-��"gk��{|�����@�I㶹1��;�~�1��s'<�=�N�ߺ�#�Ό��)���㚖H7�Kce�� 1�Vԅ0�>���������{B����Ѱp���%?S=CKA~��n�|��̸�ޅ67��-�>·�p�8X��x�IX�r�u����P1Ѡ&K���
tH���!?��3��ʢ��i:8(���Z����=���x<��&Օ�P� �b���x\D��g�A"����0q����T�W�?T��LI���1T����1��L��x���.|Jr��>a?��5��#E�d(��(�����B\�^�.�B�4$&�oQ��4�AW��6�(���gi�,���$��j�e����3S�g9P]�K 2x��A0�u���]�!Q0���6ysm��턜�ݲR�l���0�1W�`���$j�~i��28�K_�!e$�÷�S�K��_���B��Q�ܤH}����nץ7_9���D�0YB�$9�×%�ˌIe�}�y*���'2��/�o�����Z��j�-�;)Re�74IDց"B/7ү����	�'��]��dT<LXa'��3 ���B�;�n2��I�fO(��F���@�>�/L*�5�8p��w)�����y�m��:ߩ ��Y`ӻ�l��;}�7�"�"��e.z��{��C���2�D�B;/��D��E'Q����Xړg
S\��b�1�~��cb�r���O8�&ǟJ6n&��Ni �۠c=>���-��^y��h��6M��ӵ���M��~����q��e�&}L;&UEwҊ�>\�L��� Z���3,څZ݅�p�`�
�,���28�1n��MK�C�_�bIaA\w�B�H �?�-�:yy:𓟧��4!=���䛄q�e�0�=�*��-�Z�TZ�)���{E;��F#ے�0�@����kK�o�L���Â�>u�)q�>m2ɔA�9>_�A�0q��޶j����$��E;S�q�+)��'�%0ܮ�`>�����٥�7�g�E�y���)�z�"j�h�tD��w{g G�/��V/z-���q!�eE��E�5gϦGV����K��~J����]����D1�s��u��!����;�8�Zc�&읶����dz�	�d�2	��k[���HD<!��^��Nz{�?pE���l	rsgh�f����۬�I&�/�m�
Wa~/��|4d^Q�5���-$��Ap�w��o!��maȄ6�3c��i�Ȩx	]M�,����@��!Wm�ΚFP�m=|j��/�;�-����Ѭ�k %�G)�Jb�D>��zsJI`/�ar�J�Y"���Z��/��ɝ��}�+O.Z��̈́�׃..$SmP51�<w����N�MVξ̰bK���ҧIT�P�B�	�l�[�x����d��E���B�������'|�a=�k��^-l���pW�j�a�<���'+�X�������4n��B��Ԯ)���㤿�>N�����?�mM�����k��P�Z��5��_m^�j����@2�q��q����@6�����r�����C1S���/<& 
]��>�o~�*ŗUE�ƙT���a^�W�\.
0��'���q��b�]�rj����p��7�����o���`���q*Q�=���N�k��,��R�����H���ö��sC�����T9K�#u���!�$X�/���9����&��Z�b8<:�h����	/��x+=ϵڢ�p�} ig��h�����px��Q�Eװ��~ÊNԁ~�T)ښ#����&�9DQ-��!�w��UG%�2$� J�����Vsϝ��a����x�
�1$q�v�v,���J��>�m-�޿K2��2�)�S�I�8F{���ꐚ8��)�i�B�LG�4ri/������"m�Xb��d�E�7�2f�ezX�f͌��+���/y �lUem��(�[�ޕ0|� ~oT?b�y\��s��%䆲L��s��7�EW ���N�����iA�@y����,�E������&GI���+5�j�p�H"�*��^N����J����f�R�)daDǕ���_s��l�XF�z��Ȕ��@M�Ou~K�(�5(�Bi&���&��+�K�딛������y�����{�,�3�X�Zp��@@k�]�)��٫R`���o�&�9�	�!�O��3�����n9���vݧw������!%�G櫰�U{��'?��6� �'��3?�8�� :E��z��{M"����lW�����(�F�"���J�,^#��{w���lU!,�~��� x1x�G�3^b��d�=/��_Ƙ���?��t7�������~�8=@%����c�%&��[�E!>�Zj�q�Ϧ1���^D��Ʈ�?�9>�;��d�8��F�;T�TӬAӴ�,X.d~�a]l��碇9)�c<�W9p���Q5p�Z��Cj�����A���ǳ�	�1)N�J�U��=^���I�y��v��9��d��tQ���ܣk�󇏾q�f�^�_-k����B��n�v?W��^W��j�}q_v5�W<+���~,��-��G��;��ڍ��&�O�b)��?����ca4Ugu�.^����{MƎ$�R��ߵ ���v['+�w+��w���R���r�w���k�oO��z���� ! OǛ/M@�v�*�����g|i\�t[��Jg�t+�(\���/����ϺXVii�)��(��,�G��������W���	[�i:�.9*��5j��vqɯT���=]LO�����:���yU�Lqh�k/�.E��,꘺��-#�<�~��~����7'����kB��"�*��%C7�c9򯞺����p-4��HAH<rC@���Y֣q��ý�yʋ���@��/��4n����ܺ����3�������6�\`�Y����2�*Q���W�R9:����2ź\�^U�[�>�t�1�d�J>�4N�Y��^Ĩ��SZ�,���.��u��jt�G9D��j�<��L�Q�s�-:'���;f FN���\���t�h�z���/�
�2r��R��q�#����(���?Tcղ)d}0)T�\ i��/5��dbʧ"32�G��O�O�!�T��4v��d*8˥�`G���#��-Cm�K��	^*��V_�Fjθ���/ҹO���X_�K�Ο(7<�'?
jy��Z�l�|㎯$B&s��m}�e!Y�Z !���Yi׸S�]7���]Ym�;J|-���
�O�ؒ����	U�ke�V� �q�{?xן�B��i�}H#f?��K��Y��o�⶷��}y�7��)�:M+V�����~iZ؊����?A��>�~��,��_�W�ݮ׊q�Ǻ��M�]��0p��oy��9e=�a~�����k���"7�L���~4���<��Y�@_���Γa���D�JY���4|���$����$��$K��}�Pԇl�vh�~ R.�w�e�$g�������rb������؉���f��a�G{m�-�F/��~�9/(�ՙ��d6r�(��Nr��"��E�n.cF�w)�CȞ8��t�Q�1)������uB�Bj������m�y��f����|c�������v_�K4�fh09�=1��[|�����R���}��f�T
�ݣ�����j%�8H�A�m�ֳ؋�_�Q�g���Ȃ�����
[�uLig=��d���?
�Hv@�`vT�W�i�!���1ۯ�3P�S�j��p[�!z|뾡RF��{@�yn��M�|�v��*� pg�;d/�M�%x�H�� �����[����e���_��H* �����]e���X^��Ҷ䛐,�ՉOn��{ZAĸ� ]H��҉���Fq2Һ���ŀ�=C,w�� $��b��~@~�H6�H��x���#p����l�81��#�/)���$�� �	  jP���="�Ԅ2w9�3�d�9"��*|߲�m�E�*�3@�d��,a�_�?�]P8��i�U�T������Q���V��2L�@z����(Wë]˖����]~�~թ�t
�9IA�?���K��zS���xņ*6\�x1�Äe��1&~�|�"�V�deX�,*"���L5����9�Ƶ�B��p��v�J*��)tk�(9t|��'�g��d^T"3�~���2�	W0?X�m��<d�zCo:�mj���M!�:HX���;�K���"���véQ~�mW�:�p�Z7B�L�6I�7G�O���h-W�J���" ��k�>�,���dG����`��j�v���軄�Y����Y2̍{��#ĂՅ][Yo(���!P��c�(.�
u�vL����4*���]��1��G8�R]<��ʵГ��cH*�3t��ĂA�Ϫ+Ad�y�P��$12bvm'HY�<�\�|e�Y@ �$ty��1�M#�$�i	!s v\�t?��%����5���~�Dv1i�&O��8T�l�)��c�U8ͨz֣��uDNVV��&� ���\��%�F�x���b�qE~�M61�[4�"�d���<.Vi�K�eZzH�^M��Q�2��{��)�H9���t����w�� {Nj�D�Xya�'�qz٢q��k����G�^��(&���&) Zى�u��Dj#S� y	�q���e�
�����PŁT���C�l�n�Om�%������5Ձ��RG�>���U/X@[��)gG<74[CG��t�1k
�޿L�%De�� �bHe�����X�)Y�D���7s����خl�8~��l=�fh�a]��!����c 	��m"�l&ݸ�߸�6D��L�?��	��9����n�Ρ����P��ޡ�����Җ8ԩŁu��5j����w~7 �=-󔋌@]��u��4@6�]�M�+�TQ�t^-f0_�i��'���6!Ƚ��F�@=j
�^֐��?��z�s�Z"ϙJ��=�� 
�i�p{��_u5x��k�PdQ7Z����
�ɑr�z&i�)��/;�a��h��[�<��5E�{$�Ƶy)��6b��	�>������T+C����00,�f�j
�R����ڂ��p�9�r�#-v�{FE��|�k�_{���`j�ċtAR�o��U�}@5T��*��D�/@������-�$����2}+(��J��(�0����J(�� "b��Ы�=F-:i�҂��C;r�깩5��[`'z��~pq3����HJ:>7e�aK���wHzHRmӚf�D�_���?��H-3�*HL@1A��o������+�{�!i���Ɖϧ:��x�?�wzV���D�P�sb(/J{��e��~�U�"�����qS�i.���m�k����˹[f�vm�T�Z;\|-���z�s��Zy��T;B�I�#�F���>�k3ǖ�<����-T+�ksL����ɖ�(�貸=�Y���kf�Ĭ��%��^_BA�+ILq���d3ޚh��Ĩ6���i�?*	��P�=P�Mo[2ג��YЌ�)g.h~�ZY�6�_K�I蛈��ɋ��P���rЉ5�c�`���wO�Q���(�!D�N�Ɔa���`ˏ0�Y�\�?P��K�f9�b��#�g��pQ� ��_�bR�o1���n2t���nba����Z]���XpY&H�ɒ�T,nK*��%Jn��|/���������O�0p�@����MV�hf���*�9AA�A��=��0&���~��&U�i�y�ɴZǢx(ۚ��Ҳrݎ+7+��(�]�V�w�G�BՇ�[Ɖް�HH1���eA��
>�d[����w��r�>�f -<"`d��;Z�B$��A�xp��#��VS����K�L��KE�G<�(�������l~o����jH��z��	�M����������~Q�mtrtyG��Dm��e-��S��U6��^7\�6�'[�/[���`�ӌ;4��W�iZ���h���ZL"�Y���� �kݐ�Y$�7i)/:���Z�\��4���0�8uq��;�&�I�� +��MN三�R_��8��av+��M⋠�Ӷ�m{oC�lu-5{��Tf�2yi�r]�2�� ���p���x�+��ilvk��|�2ñY��:�)�bE8�_V.U���	i����Z��o�h�%OW	�y�:�;uE�E����l�g��������p<�h��.�9��4��fR��2�����1�	 #[�&�w	_���5%��%�[Z�$ԗ@[Ӵ�����h�6zS]ν�2�)�M�$-��ev'zv��ifJ~	S�_�W��^l6����4���0���4��k�'݀�f`Rt3�$+w{T�"�=����_�@���,���03���h��n��X-��C�;�u��^�Y˓;G1���Q�j�ϥ��;��?�����/^ƣ�      #      x�3��44�442 ���� �=... ;��         �   x�M�Kj�0D��)r��Kwɦ[j�/r�f�<(�WR9��?N��r��s�,�)�G���)��՘1�Lkk��\�ݏǛ-d�֠�Aθ�Oٕ�� �*J��u� Jښ�i�9*�0^�c	-Ę4�]��Z���<�4FKyf@ C\����Gi2�gE��!q��6H ���ݞ+aOf��61A�������P�     