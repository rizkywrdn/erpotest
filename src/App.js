import React from 'react';
import { Router, Route, Switch, Redirect, Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { history } from './_helpers';
import { alertActions, userActions, walletActions } from './_actions';
import { PrivateRoute, NumberFormat } from './_components';

import { HomePage } from './HomePage';
import { ProductPage } from './ProductPage';
import { ProfilePage } from './ProfilePage';
import { HistoryPage } from './HistoryPage';

import { LoginPage } from './LoginPage';
import { RegisterPage } from './RegisterPage';

class App extends React.Component {
    constructor(props) {
        super(props);
        history.listen((location, action) => {
            this.props.clearAlerts();
        });
    }

    componentDidMount() {
        this.props.getWallet();
    }

    logout(){
        this.props.logout();
    }

    render() {
        const loggingIn = localStorage.getItem('user');
        const { alert, user, wallet } = this.props;

        return (
            <Router history={history}>
                {loggingIn &&
                <nav className="navbar navbar-default">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <Link to="/" className="navbar-brand">Erpo Technical</Link>
                        </div>
                        <div id="navbar" className="navbar-collapse collapse">
                          <div className="navbar-inner">
                          
                            <ul className="nav navbar-nav">
                              <li><Link to="/">Home</Link></li>
                              <li><Link to="/product">Product</Link></li>
                              <li><Link to="/transaksi">History</Link></li>
                            </ul>
                            <ul className="nav navbar-nav navbar-right">
                                <li><h4><span className="label label-info">{NumberFormat(wallet.topup - wallet.purchase)}</span></h4></li>
                                <li><Link to="/profile"><b>Hi, {user.nama}</b></Link></li>
                                <li><Link to="/login">Logout</Link></li>
                            </ul>
                          </div>
                        </div>
                    </div>
                </nav>
                }
                <div className="container">
                    {alert.message &&
                        <div className={`alert ${alert.type}`}>{alert.message}</div>
                    }
                    
                    <Switch>
                        <PrivateRoute exact path="/" component={HomePage} />
                        <PrivateRoute exact path="/product" component={ProductPage} />
                        <PrivateRoute exact path="/profile" component={ProfilePage} />
                        <PrivateRoute exact path="/transaksi" component={HistoryPage} />
                        <Route path="/login" component={LoginPage} />
                        <Route path="/register" component={RegisterPage} />
                        <Redirect from="*" to="/" />
                    </Switch>
                </div>
            </Router>
        );
    }
}

function mapState(state) {
    const { alert, authentication, wallet } = state;
    const { user } = authentication;
    const { loggingIn } = state.authentication;
    return { alert, loggingIn, user, wallet };
    
}

const actionCreators = {
    clearAlerts: alertActions.clear,
    getWallet: walletActions.getAll,
    logout: userActions.logout
};

const connectedApp = connect(mapState, actionCreators)(App);
export { connectedApp as App };