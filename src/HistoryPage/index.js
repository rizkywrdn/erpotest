import React from 'react';
import { connect } from 'react-redux';
import { transaksiActions } from '../_actions';
import { NumberFormat } from '../_components';

class HistoryPage extends React.Component {

    componentDidMount() {
        this.props.getTransaksi();
    }
    render() {
        const { transaksi } = this.props;
        return (
          <div className="col-md-6 col-md-offset-3">
          <div className="page-header">
            <h3>History <small>History your transaksi</small></h3>
          </div>
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Total</th>
                  <th>Type</th>
                </tr>
              </thead>
              <tbody>
                {transaksi.loading && <tr><td colSpan="7" className="text-center">Loading History...</td></tr>}
                {transaksi.error && <span className="text-danger">ERROR: {transaksi.error}</span>}
                {transaksi.items === false && <tr><td colSpan="7" className="text-center">Empty Data...</td></tr>}
                {transaksi.items && transaksi.items.map((transaksi, index) =>
                <tr key={index}>
                  <th scope="row">{index+1+'.'}</th>
                  <td>{NumberFormat(transaksi.total)}</td>
                  <td>{transaksi.type}</td>
                </tr>
                )}
              </tbody>
            </table>
          </div>
        );
    }
}

function mapState(state) {
    const { transaksi, authentication } = state;
    const { user } = authentication;
    return { user, transaksi };
}

const actionCreators = {
    getTransaksi: transaksiActions.getAll,
}

const connectedHistoryPage = connect(mapState, actionCreators)(HistoryPage);
export { connectedHistoryPage as HistoryPage };