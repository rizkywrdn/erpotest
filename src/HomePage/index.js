import React from 'react';
import { connect } from 'react-redux';
import { NumberFormat } from '../_components';
import { productActions, transaksiActions, walletActions } from '../_actions';

class HomePage extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.props.getProduct();
        this.props.getTransaksi();
        this.props.getWallet();
    }

    handleSubmit(product) {
        return (e) => {
            const transaksi = {
                user_id: this.props.user.id,
                total: product.harga,
                type: 'PURCHASE',
                product_id: product.id,
                sku: product.sku
            }
            const wallets = this.props.wallet.topup - this.props.wallet.purchase;
            if (parseInt(product.sku) < 0) {
                alert('Out of stock');
            }else if(parseInt(product.harga) > parseInt(wallets)) {
                alert('Not enough Wallet');
            }else{
                this.props.purchase(transaksi);
            }
        }
    }

    render() {
        const { products } = this.props;
        return (
            
            <div className="row">
              <div className="page-header">
                <h3>Homepage <small>Browser your product</small></h3>
              </div>
            {products.items && products.items.map((product, index) =>
              <div className="col-sm-6 col-md-3" key={index}>
                <div className="thumbnail">
                  <img src={product.gambar} width="100%" alt={product.nama_produk}/>
                  <div className="caption">
                    <h3>{product.nama_produk}</h3>
                    <p>{NumberFormat(product.harga)}</p>
                    <p><button className="btn btn-primary btn-sm" onClick={this.handleSubmit(product)}>Purchase</button></p>
                  </div>
                </div>
              </div>
              )}
            </div>
        );
    }
}

function mapState(state) {
    const { transaksi, products, authentication, wallet } = state;
    const { user } = authentication;
    return { user, products, transaksi, wallet };
}

const actionCreators = {
    getProduct: productActions.getAll,
    getTransaksi: transaksiActions.getAll,
    getWallet: walletActions.getAll,
    purchase: transaksiActions.created
}

const connectedHomePage = connect(mapState, actionCreators)(HomePage);
export { connectedHomePage as HomePage };