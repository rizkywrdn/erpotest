import React from 'react';
import { Link } from 'react-router-dom';

class ProductForm extends React.Component {
    constructor(props) {
        super(props);
        const result = props.product;
        this.state = {
            product: {
                user_id: props.user.id,
                nama_produk: (result.length === 0 ? '' : result.nama_produk),
                sku: (result.length === 0 ? '' : result.sku),
                gambar: '',
                deskripsi: (result.length === 0 ? '' : result.deskripsi),
                harga: (result.length === 0 ? '' : result.harga),
                id: (result.length === 0 ? '' : result.id),
            },
            submitted: false
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this._handleImageChange = this._handleImageChange.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        const { product } = this.state;
        this.setState({
            product: {
                ...product,
                [name]: value
            }
        });
    }

    _handleImageChange(e) {
      e.preventDefault();
      let reader = new FileReader();
      let file = e.target.files[0];
      const { product } = this.state;
      reader.onloadend = () => {
        this.setState({
          product: {
            ...product,
            gambar: reader.result
          }
        });
      }
      reader.readAsDataURL(file)
    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({ submitted: true });
        const { product } = this.state;
        if (product.nama_produk && product.sku && product.gambar && product.deskripsi && product.harga) {
           this.props.handleSubmit(product);
        }
    }

    render() {
        const { product, submitted } = this.state;
        return (
          <div className="col-md-6 col-md-offset-3">
            <h2>{product.id ? 'Product Edit' : 'Product Add'}</h2>
            <form name="form" onSubmit={this.handleSubmit}>
              <div  className={'form-group' + (submitted && !product.nama_produk ? ' has-error' : '')}>
                  <label htmlFor="nama_produk">Name Product</label>
                  <input type="text" className="form-control" name="nama_produk" value={product.nama_produk} onChange={this.handleChange} />
                  {submitted && !product.nama_produk &&
                      <div className="help-block">Name Product is required</div>
                  }
              </div>
              <div className={'form-group' + (submitted && !product.sku ? ' has-error' : '')}>
                  <label htmlFor="sku">SKU</label>
                  <input type="number" className="form-control" name="sku" value={product.sku} onChange={this.handleChange} />
                  {submitted && !product.sku &&
                      <div className="help-block">SKU is required</div>
                  }
              </div>
              <div className={'form-group' + (submitted && !product.gambar ? ' has-error' : '')}>
                  <label htmlFor="gambar">Image</label>
                  <input type="file" className="form-control" name="gambar" onChange={this._handleImageChange} />
                  {submitted && !product.gambar &&
                      <div className="help-block">Image is required</div>
                  }
              </div>
              <div className={'form-group' + (submitted && !product.deskripsi ? ' has-error' : '')}>
                  <label htmlFor="sku">Description</label>
                  <textarea className="form-control" name="deskripsi" value={product.deskripsi} onChange={this.handleChange} />
                  {submitted && !product.deskripsi &&
                      <div className="help-block">Description is required</div>
                  }
              </div>
              <div className={'form-group' + (submitted && !product.harga ? ' has-error' : '')}>
                  <label htmlFor="sku">Price</label>
                  <input type="number" className="form-control" name="harga" value={product.harga} onChange={this.handleChange} />
                  {submitted && !product.harga &&
                      <div className="help-block">Price is required</div>
                  }
              </div>
              <div className="form-group">
                  <button className="btn btn-primary">Save</button> 
                  <Link to="/product" className="btn btn-link" onClick={() => this.props.handleCancle()}>Cancel</Link>                 
              </div>
            </form>
          </div>
        );
    }
}

export default ProductForm;