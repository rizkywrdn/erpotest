import React from 'react';
import { Link } from 'react-router-dom';
import { NumberFormat } from '../_components';

class ProductList extends React.Component {

    render() {
        const { products } = this.props;
        return (
          <div>
            <div className="page-header">
              <h3>Product List <small>Your product list</small></h3>
            </div>
            <p><button onClick={() => this.props.AddProduct()} className="btn btn-primary">Add Product</button> <Link to="/" className="btn btn-success" role="button">Import Product API</Link>
            </p>
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nama Produk</th>
                  <th>SKU</th>
                  <th>Gambar</th>
                  <th>Deskripsi</th>
                  <th>Harga</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {products.loading && <tr><td colSpan="7" className="text-center">Loading products...</td></tr>}
                {products.error && <span className="text-danger">ERROR: {products.error}</span>}
                {products.items === false && <tr><td colSpan="7" className="text-center">Empty products...</td></tr>}
                {products.items && products.items.map((product, index) =>
                <tr key={index}>
                  <th scope="row" width="1%">{index+1+'.'}</th>
                  <td width="25%">{product.nama_produk}</td>
                  <td width="10%">{product.sku}</td>
                  <td width="100"><img src={product.gambar} width="50" alt={product.nama_produk}/></td>
                  <td width="25%">{product.deskripsi}</td>
                  <td width="10%">{NumberFormat(product.harga)}</td>
                  <td width="13%">
                    <button className="btn btn-info" onClick={this.props.edit(product)}>Edit</button>&nbsp;
                    {
                        product.deleting ? <button className="btn btn-danger">Deleting...</button>
                        : product.deleteError ? <span className="text-danger"> - ERROR: {product.deleteError}</span>
                        : <button className="btn btn-danger" onClick={this.props.deleted(product.id)}>Delete</button>
                    }
                    
                  </td>
                </tr>
                )}

              </tbody>
            </table>
          </div>
        );
    }
}

export default ProductList;