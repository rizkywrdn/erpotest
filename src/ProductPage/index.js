import React from 'react';
import { connect } from 'react-redux';

import { productActions } from '../_actions';

import ProductList from './ProductList';
import ProductForm from './ProductForm';

class ProductPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          result: [],
          isAddProduct: false,
          isEditProduct: false
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleCreate = this.handleCreate.bind(this);
        this.handleEdit   = this.handleEdit.bind(this);
        this.handleCancle = this.handleCancle.bind(this);
    }

    componentDidMount() {
        this.props.getProduct();
    }

    handleSubmit(product){
        if(this.state.isAddProduct){
            this.setState({ 
                isAddProduct: false, 
                isEditProduct: false 
            }, this.props.createProduct(product));
            
        }else{
            this.setState({ 
                isAddProduct: false, 
                isEditProduct: false 
            }, this.props.updateProduct(product));            
        }
        
    }

    handleDelete(id) {
        return (e) => this.props.deleteProduct(id);
    }

    handleCreate() {
        this.setState({ isAddProduct: true });
    }

    handleEdit(product) {
        return (e) => this.setState({ isEditProduct: true, result: product });
    }

    handleCancle() {
        this.setState({ isAddProduct: false, isEditProduct: false });
    }

    render() {
        if(this.state.isAddProduct || this.state.isEditProduct){
            return (
                <ProductForm 
                    user={this.props.user}
                    product={this.state.result}
                    handleSubmit={this.handleSubmit}
                    handleCancle={this.handleCancle}
                />
            )
        }else{
            return (
                <ProductList 
                    products={this.props.products} 
                    deleted={this.handleDelete} 
                    edit={this.handleEdit} 
                    AddProduct={this.handleCreate}
                />
            );
        }
    }
}

function mapState(state) {
    const { products, authentication } = state;
    const { user } = authentication;
    return { products, user };
}

const actionCreators = {
    getProduct: productActions.getAll,
    deleteProduct: productActions.delete,
    createProduct: productActions.created,
    updateProduct: productActions.updated,
}

const connectedProductPage = connect(mapState, actionCreators)(ProductPage);
export { connectedProductPage as ProductPage };