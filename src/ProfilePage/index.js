import React from 'react';
import { connect } from 'react-redux';
import { transaksiActions } from '../_actions';

class ProfilePage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            transaksi: {
                user_id: props.user.id,
                total: '',
                type: 'TOPUP'
            },
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        const { transaksi } = this.state;
        this.setState({
            transaksi: {
                ...transaksi,
                [name]: value
            }
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({ submitted: true });
        const { transaksi } = this.state;
        if (transaksi.total) {
            this.props.topUp(transaksi);
        }
    }

    render() {
        const { transaksi, submitted } = this.state;
        const { user } = this.props;
        return (
          <div className="col-md-6 col-md-offset-3">
            <div className="page-header">
              <h3>Profile <small>Your account</small></h3>
            </div>
            <table className="table table-striped">
              <tbody>
                <tr>
                  <th scope="row">Name</th>
                  <td>:</td>
                  <td>{user.nama}</td>
                </tr>
                <tr>
                  <th scope="row">Username</th>
                  <td width="5%">:</td>
                  <td>{user.username}</td>
                </tr>
                <tr>
                  <th scope="row">Your Wallet</th>
                  <td width="5%">:</td>
                  <td>{user.username}</td>
                </tr>
              </tbody>
            </table>
            <form name="form" onSubmit={this.handleSubmit}>
              <div className={'input-group' + (submitted && !transaksi.total ? ' has-error' : '')}>
                <input type="number" className="form-control" name="total"  value={transaksi.total} onChange={this.handleChange} placeholder="Wallet TopUp" />
                <span className="input-group-btn">
                  <button className="btn btn-primary">Top Up!</button>
                </span>
              </div>
              {submitted && !transaksi.total &&
                  <div className="help-block">TopUp is required</div>
                }
            </form>
          </div>
        );
    }
}

function mapState(state) {
    const { authentication } = state;
    const { user } = authentication;
    return { user };
}

const actionCreators = {
    topUp: transaksiActions.created
}

const connectedProfilePage = connect(mapState, actionCreators)(ProfilePage);
export { connectedProfilePage as ProfilePage };