import { productConstants } from '../_constants';
import { productService } from '../_services';
import { alertActions } from './';
import { history } from '../_helpers';

export const productActions = {
    created,
    updated,
    getAll,
    delete: _delete
};

function created(product) {
    return dispatch => {
        dispatch(request(product));

        productService.created(product)
            .then(
                products => { 
                    if(products){
                        dispatch(success(products));
                        history.push('/product');
                        dispatch(alertActions.success('Product added successful'));
                    }else{
                        dispatch(failure('Product not added successful'));
                        dispatch(alertActions.error('Product not added successful'));
                    }
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(products) { return { type: productConstants.CREATED_REQUEST, products } }
    function success(products) { return { type: productConstants.CREATED_SUCCESS, products } }
    function failure(error) { return { type: productConstants.CREATED_FAILURE, error } }
}

function updated(product) {
    return dispatch => {
        dispatch(request(product));

        productService.updated(product)
            .then(
                product => { 
                    if(product){
                        dispatch(success(product));
                        window.location.reload(true);
                        dispatch(alertActions.success('Product updated successful'));
                    }else{
                        dispatch(failure('Product not updated successful'));
                        dispatch(alertActions.error('Product not updated successful'));
                    }
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(product) { return { type: productConstants.UPDATED_REQUEST, product } }
    function success(product) { return { type: productConstants.UPDATED_SUCCESS, product } }
    function failure(error) { return { type: productConstants.UPDATED_FAILURE, error } }
}

function getAll() {
    return dispatch => {
        dispatch(request());

        productService.getAll()
            .then(
                products => dispatch(success(products)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: productConstants.GETALL_REQUEST } }
    function success(products) { return { type: productConstants.GETALL_SUCCESS, products } }
    function failure(error) { return { type: productConstants.GETALL_FAILURE, error } }
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
    return dispatch => {
        dispatch(request(id));

        productService.delete(id)
            .then(
                product => dispatch(success(id)),
                error => dispatch(failure(id, error.toString()))
            );
    };

    function request(id) { return { type: productConstants.DELETE_REQUEST, id } }
    function success(id) { return { type: productConstants.DELETE_SUCCESS, id } }
    function failure(id, error) { return { type: productConstants.DELETE_FAILURE, id, error } }
}