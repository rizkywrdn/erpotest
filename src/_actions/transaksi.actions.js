import { transaksiConstants } from '../_constants';
import { transaksiService } from '../_services';
import { alertActions } from './';
import { history } from '../_helpers';

export const transaksiActions = {
    created,
    updated,
    getAll,
    delete: _delete
};

function created(transaksi) {
    return dispatch => {
        dispatch(request(transaksi));

        transaksiService.created(transaksi)
            .then(
                transaksi => { 
                    if(transaksi){
                        dispatch(success(transaksi));
                        history.push('/transaksi');
                        window.location.reload(true);
                        dispatch(alertActions.success('transaksi added successful'));
                    }else{
                        dispatch(failure('transaksi not added successful'));
                        dispatch(alertActions.error('transaksi not added successful'));
                    }
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(transaksi) { return { type: transaksiConstants.CREATED_REQUEST, transaksi } }
    function success(transaksi) { return { type: transaksiConstants.CREATED_SUCCESS, transaksi } }
    function failure(error) { return { type: transaksiConstants.CREATED_FAILURE, error } }
}

function updated(transaksi) {
    return dispatch => {
        dispatch(request(transaksi));

        transaksiService.updated(transaksi)
            .then(
                transaksi => { 
                    if(transaksi){
                        dispatch(success(transaksi));
                        window.location.reload(true);
                        dispatch(alertActions.success('transaksi updated successful'));
                    }else{
                        dispatch(failure('transaksi not updated successful'));
                        dispatch(alertActions.error('transaksi not updated successful'));
                    }
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(transaksi) { return { type: transaksiConstants.UPDATED_REQUEST, transaksi } }
    function success(transaksi) { return { type: transaksiConstants.UPDATED_SUCCESS, transaksi } }
    function failure(error) { return { type: transaksiConstants.UPDATED_FAILURE, error } }
}

function getAll() {
    return dispatch => {
        dispatch(request());
        transaksiService.getAll()
            .then(
                transaksi => dispatch(success(transaksi)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: transaksiConstants.GETALL_REQUEST } }
    function success(transaksi) { return { type: transaksiConstants.GETALL_SUCCESS, transaksi } }
    function failure(error) { return { type: transaksiConstants.GETALL_FAILURE, error } }
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
    return dispatch => {
        dispatch(request(id));

        transaksiService.delete(id)
            .then(
                transaksi => dispatch(success(id)),
                error => dispatch(failure(id, error.toString()))
            );
    };

    function request(id) { return { type: transaksiConstants.DELETE_REQUEST, id } }
    function success(id) { return { type: transaksiConstants.DELETE_SUCCESS, id } }
    function failure(id, error) { return { type: transaksiConstants.DELETE_FAILURE, id, error } }
}