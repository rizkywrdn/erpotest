import { walletConstants } from '../_constants';
import { walletService } from '../_services';

export const walletActions = {
    getAll
};

function getAll() {
    return dispatch => {
        dispatch(request());
        walletService.getAll()
            .then(
                wallet => dispatch(success(wallet)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: walletConstants.GETALL_REQUEST } }
    function success(wallet) { return { type: walletConstants.GETALL_SUCCESS, wallet } }
    function failure(error) { return { type: walletConstants.GETALL_FAILURE, error } }
}
