export const NumberFormat = (value) => {
  const angka = new Intl.NumberFormat('de-ID').format(value);
  return 'Rp '+angka;
}