import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { users } from './users.reducer';
import { products } from './products.reducer';
import { transaksi } from './transaksi.reducer';
import { wallet } from './wallet.reducer';
import { alert } from './alert.reducer';

const rootReducer = combineReducers({
  authentication,
  registration,
  users,
  products,
  transaksi,
  wallet,
  alert
});

export default rootReducer;