import { transaksiConstants } from '../_constants';

export function transaksi(state = {}, action) {
  switch (action.type) {
    case transaksiConstants.GETALL_REQUEST:
      return {
        loading: true
      };
    case transaksiConstants.GETALL_SUCCESS:
      return {
        items: action.transaksi
      };
    case transaksiConstants.GETALL_FAILURE:
      return { 
        error: action.error
      };

    case transaksiConstants.CREATED_REQUEST:
      return {
        ...state,
        items: state.items.map(transaksi =>
          transaksi.id === action.id
            ? { ...transaksi }
            : transaksi
        )
      };
    case transaksiConstants.CREATED_SUCCESS:
      return {
        items: [...state.items, action.transaksi]
      };
    case transaksiConstants.CREATED_FAILURE:
      return { 
        error: action.error
      };

    case transaksiConstants.UPDATED_REQUEST:
      return {
        ...state,
        items: state.items.map(transaksi =>
          transaksi.id === action.id
            ? { ...transaksi }
            : transaksi
        )
      };
    case transaksiConstants.UPDATED_SUCCESS:
      return {
        items: state.items.map(transaksi => {
          if (transaksi.id !== action.id) {
            return transaksi;
          } else {
            return { ...transaksi};
          }
        })
      };
    case transaksiConstants.UPDATED_FAILURE:
      return { 
        error: action.error
      };

    case transaksiConstants.DELETE_REQUEST:
      // add 'deleting:true' property to transaksi being deleted
      return {
        ...state,
        items: state.items.map(transaksi =>
          transaksi.id === action.id
            ? { ...transaksi, deleting: true }
            : transaksi
        )
      };
    case transaksiConstants.DELETE_SUCCESS:
      // remove deleted transaksi from state
      return {
        items: state.items.filter(transaksi => transaksi.id !== action.id)
      };
    case transaksiConstants.DELETE_FAILURE:
      // remove 'deleting:true' property and add 'deleteError:[error]' property to transaksi 
      return {
        ...state,
        items: state.items.map(transaksi => {
          if (transaksi.id === action.id) {
            // make copy of transaksi without 'deleting:true' property
            const { deleting, ...transaksiCopy } = transaksi;
            // return copy of transaksi with 'deleteError:[error]' property
            return { ...transaksiCopy, deleteError: action.error };
          }

          return transaksi;
        })
      };
    default:
      return state
  }
}