import { walletConstants } from '../_constants';

export function wallet(state = {}, action) {
  switch (action.type) {
    case walletConstants.GETALL_REQUEST:
      return {
        loading: true
      };
    case walletConstants.GETALL_SUCCESS:
      return action.wallet
    case walletConstants.GETALL_FAILURE:
      return { 
        error: action.error
      };

    default:
      return state
  }
}